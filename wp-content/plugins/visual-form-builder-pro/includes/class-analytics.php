<?php
/**
 * Class that builds our Entries table
 * 
 * @since 1.2
 */
class VisualFormBuilder_Pro_Analytics {

	public function __construct(){
		global $wpdb;
		
		// Setup global database table names
		$this->field_table_name 	= $wpdb->prefix . 'vfb_pro_fields';
		$this->form_table_name 		= $wpdb->prefix . 'vfb_pro_forms';
		$this->entries_table_name 	= $wpdb->prefix . 'vfb_pro_entries';
		
		add_action( 'admin_init', array( &$this, 'display' ) );
	}
	
	public function display(){
		global $wpdb;
		
		
		// Query to get all forms
		$order = sanitize_sql_orderby( 'form_id ASC' );
		$where = apply_filters( 'vfb_pre_get_forms_analytics', '' );
		$forms = $wpdb->get_results( "SELECT * FROM $this->form_table_name WHERE 1=1 $where ORDER BY $order" );
		
		if ( !$forms ) :
			echo '<div class="vfb-form-alpha-list"><h3 id="vfb-no-forms">You currently do not have any forms.  Click on the <a href="' . esc_url( admin_url( 'admin.php?page=vfb-add-new' ) ) . '">New Form</a> button to get started.</h3></div>';
		
		else :
		
		$form_nav_selected_id = ( isset( $_REQUEST['form_id'] ) ) ? absint( $_REQUEST['form_id'] ) : $forms[0]->form_id;
		
		$entries = $wpdb->get_results( "SELECT DAY( date_submitted ) AS Day, MONTH( date_submitted ) AS Month, YEAR( date_submitted ) AS Year, COUNT(*) AS Count FROM $this->entries_table_name WHERE form_id = $form_nav_selected_id GROUP BY Day ORDER BY Count DESC" );

		?>
        
        <form method="post" id="analytics-switcher">
            <label for="form_id"><p style="margin:8px 0;"><em><?php _e( 'Select which form analytics to view', 'visual-form-builder-pro' ); ?>:</em></p></label> 
            <select name="form_id">
		<?php
		$count = $sum = $avg = 0;
		foreach ( $forms as $form ) {
			if ( $form_nav_selected_id == $form->form_id ) {
				$count = count( $entries );
				$busy_date = date( 'M d, Y', mktime( 0, 0, 0, $entries[0]->Month, $entries[0]->Day, $entries[0]->Year ) );
				$busy_count = $entries[0]->Count;
				
				foreach ( $entries as $entry ) {
					$sum += $entry->Count;
				}
				
				$avg = round( $sum / $count );
			}
			
			echo '<option value="' . $form->form_id . '"' . selected( $form->form_id, $form_nav_selected_id, 0 ) . ' id="' . $form->form_key . '">' . stripslashes( $form->form_title ) . '</option>';
		}
?>
		</select>
		<?php submit_button( __( 'Select', 'visual-form-builder-pro' ), 'secondary', 'submit', false ); ?>
        </form>

        <div id="nav-menus-frame">
            <div id="menu-settings-column" class="metabox-holder">
                <div class="analytics-meta-boxes">
                    <h1><?php _e( 'Entries Total', 'visual-form-builder-pro' ); ?></h1>
                    <h2><?php echo $sum; ?></h2>
                </div>
                <div class="analytics-meta-boxes">
                    <h1><?php _e( 'Average per Day', 'visual-form-builder-pro' ); ?></h1>
                    <h2><?php echo $avg; ?></h2>
                </div>
                <div class="analytics-meta-boxes">
                    <h1><?php _e( 'Your Busiest Day', 'visual-form-builder-pro' ); ?></h1>
                    <h2><?php echo $busy_date; ?></h2>
					<h3><?php echo $busy_count; ?> <?php _e( 'Entries', 'visual-form-builder-pro' ); ?></h3>
                </div>
            </div>
            
            <div id="menu-management-liquid" class="charts-container">
                <div class="charts-nav">
                    <a class="" href="#days"><?php _e( 'Days', 'visual-form-builder-pro' ); ?></a>
                    <a class="" href="#weeks"><?php _e( 'Weeks', 'visual-form-builder-pro' ); ?></a>
                    <a class="current" href="#months"><?php _e( 'Months', 'visual-form-builder-pro' ); ?></a>
                </div>
                
                <h2><?php _e( 'Overview', 'visual-form-builder-pro' ); ?></h2>
                <div id="chart_div">
                	<div class="chart-loading"><?php _e( 'Loading', 'visual-form-builder-pro' ); ?>... <img id="chart-loading" alt="" src="<?php echo admin_url( '/images/wpspin_light.gif' ); ?>" class="waiting spinner" /></div>
                </div>
                <br class="clear" />
                
                <h2><?php _e( 'Percentage Change Over Time', 'visual-form-builder-pro' ); ?></h2>
                <div id="data_table">
                	<div class="chart-loading"><?php _e( 'Loading', 'visual-form-builder-pro' ); ?>... <img id="table-loading" alt="" src="<?php echo admin_url( '/images/wpspin_light.gif' ); ?>" class="waiting spinner" /></div>
                </div>
            </div>
        </div>
<?php
		endif;
	}
}
?>