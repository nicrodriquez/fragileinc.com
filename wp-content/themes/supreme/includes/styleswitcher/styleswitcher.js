var $j = jQuery.noConflict();

var onLoad = {
    init: function(){
    	$j('body').append('<div class="style-switcher"><h4>Style Switcher<a class="switch-button"><i class="icon-cog"></i></a></h4><div class="switch-cont"><h5>Layout options</h5><ul class="options layout-select"><li><a class="boxed" href="#"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/includes/styleswitcher/page-bordered.png"/></a></li><li class="selected"><a class="fullwidth" href="#"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/includes/styleswitcher/page-fullwidth.png"/></a></li></ul><ul class="options ad-select"><li><h5>Sitewide Advert</h5><div class="onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="adswitch" checked><label class="onoffswitch-label" for="adswitch"><div class="onoffswitch-inner"></div><div class="onoffswitch-switch"></div></label></div></li><li><h5>Header Advert/Text</h5><div class="onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="headerswitch" checked><label class="onoffswitch-label" for="headerswitch"><div class="onoffswitch-inner"></div><div class="onoffswitch-switch"></div></label></div></li></ul><h5>Background (boxed-only)</h5><ul class="options bg-select"><li><a href="#" data-bgimage="tileable_wood_texture.png" class="pattern"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/images/preset-backgrounds/tileable_wood_texture.png" alt="wood"/></a></li><li><a href="#" data-bgimage="detailed.png" class="pattern"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/images/preset-backgrounds/detailed.png" alt="detailed"/></a></li><li><a href="#" data-bgimage="px_by_Gre3g.png" class="pattern"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/images/preset-backgrounds/px_by_Gre3g.png" alt="pixels"/></a></li><li><a href="#" data-bgimage="diagonal-noise.png" class="pattern"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/images/preset-backgrounds/diagonal-noise.png" alt="diagonal-noise"/></a></li><li><a href="#" data-bgimage="dark_wood.png" class="pattern"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/images/preset-backgrounds/dark_wood.png" alt="dark_wood"/></a></li><li><a href="#" data-bgimage="swoosh_b&w.jpg" class="cover"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/includes/styleswitcher/swoosh_b&w_thumb.jpg" alt="swoosh_b&w"/></a></li><li><a href="#" data-bgimage="swoosh_colour.jpg" class="cover"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/includes/styleswitcher/swoosh_colour_thumb.jpg" alt="swoosh_colour"/></a></li><li><a href="#" data-bgimage="beach.jpg" class="cover"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/includes/styleswitcher/beach_thumb.jpg" alt="beach"/></a></li><li><a href="#" data-bgimage="L1040896.jpg" class="cover"><img src="http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/includes/styleswitcher/L1040896_thumb.jpg" alt="sundown"/></a></li></ul><a class="many-more" href="http://themes.swiftpsd.com/supreme/unlimited-colors/">More options included &rarr;</a></div></div>')
    	
		$j('.style-switcher').on('click', 'a.switch-button', function(e) {
			e.preventDefault();
			var $style_switcher = $j('.style-switcher');
			if ($style_switcher.css('left') == '0px') {
				$style_switcher.animate({
					left: '-240'
				});
			} else {
				$style_switcher.animate({
					left: '0'
				});
			}
		});

		$j('.layout-select li').on('click', 'a', function(e) {
			e.preventDefault()
			$j('.layout-select li').removeClass('selected');
			$j(this).parent().addClass('selected');
			var selectedLayout = $j(this).attr('class');
			
			if (selectedLayout == "boxed") {
				$j("#container").addClass('boxed-layout');
				$j("#top-bar").addClass('boxed-layout');
			} else {
				$j("#container").removeClass('boxed-layout');
				$j("#top-bar").removeClass('boxed-layout');
			}
			
			$j('.flexslider').each(function() {
				var slider = $j(this).data('flexslider');
				if (slider) {
				slider.resize();
				}
			});
			$j(window).resize();
		});
		
		$j('#adswitch').change(function() {
			$j('#sitewide-ad').slideToggle();
		});
		
		var headerAd = $j('.header-advert').html();
		var headerText = '<h5 style="padding-top:10px;margin-bottom:0;">CHAMPIONING CREATIVITY ACROSS<br/> THE ART AND DESIGN WORLD</h5>';
		
		$j('#headerswitch').change(function() {
			if ($j(this).is(':checked')) {
				$j('.header-advert').html(headerAd);
			} else {
				$j('.header-advert').html(headerText);
			}
		});
		
		$j('.bg-select li').on('click', 'a', function(e) {
			e.preventDefault();
			var newBackground = $j(this).attr('data-bgimage'),
				bgType = $j(this).attr('class');
					
			if (bgType == "cover") {
				$j('body').css('background', 'url(http://themes.swiftpsd.com/shared/styleswitcher/'+newBackground+') no-repeat center top fixed');
				$j('body').css('background-size', 'cover');
			} else {
				$j('body').css('background', 'url(http://themes.swiftpsd.com/supreme/wp-content/themes/supreme/images/preset-backgrounds/'+newBackground+') repeat center top fixed');
				$j('body').css('background-size', 'auto');
			}
		});
    }
};

$j(document).ready(onLoad.init);